<?php

namespace App\Tests\Infrastructure\Controller;

use App\Application\UseCase\CreateProduct\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

class ProductControllerTest extends WebTestCase
{
    protected static $application;

    public function setUp()
    {
        parent::setUp();
        passthru('php bin/console doctrine:database:create --env=test --if-not-exists');
        passthru('php bin/console doctrine:schema:update --force --env=test');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        passthru('php bin/console doctrine:schema:drop --force --env=test');
    }

    public function testCreateProduct_Successful()
    {
        $productCode = 'PRD_1';
        $productTitle = 'Продукт 1';

        $requestData = [
            'title' => $productTitle,
            'code' => $productCode,
        ];

        $client = static::createClient();

        $client->request(
            'POST',
            '/api/products',
            [],
            [],
            [],
            json_encode($requestData)
        );

        $response = $client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);
        // ответ сервера ['id' => ...]
        $createdProductId  = $content['id'];

        /** @var ProductRepository $productRepository */
        $productRepository = self::$container->get(ProductRepository::class);

        $productFound = $productRepository->findById($createdProductId);

        $this->assertEquals($productTitle, $productFound->getTitle());
        $this->assertEquals($productCode, $productFound->getCode());
    }
}
