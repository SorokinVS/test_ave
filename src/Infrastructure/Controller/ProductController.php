<?php

namespace App\Infrastructure\Controller;

use App\Application\UseCase\CreateProduct\CreateProduct;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Routing\Annotation\Route;

class ProductController
{
    /**
     * @var MessageBusInterface
     */
    private $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/api/products", name="create_product")
     */
    public function create(Request $request): JsonResponse
    {
        $createProductCommand = new CreateProduct($request);
        $result = $this->bus->dispatch($createProductCommand);
        $productId = $result->last(HandledStamp::class);

        return new JsonResponse(['id' => $productId->getResult()]);
    }
}
