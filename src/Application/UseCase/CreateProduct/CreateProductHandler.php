<?php

namespace App\Application\UseCase\CreateProduct;

use App\Domain\Model\Product;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class CreateProductHandler implements MessageHandlerInterface
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Создает продукт по команде, возвращает ид этого продукта.
     *
     * @param CreateProduct $command
     *
     * @return string ид продукта
     */
    public function __invoke(CreateProduct $command): string
    {
        $product = new Product(uniqid(), $command->getTitle(), $command->getCode());
        $this->productRepository->save($product);
        return $product->getId();
    }
}
