### Предложения по улучшению кода
* Необходимо обеспечить валидацию входящих данных
* Необходимо обеспечить внятную систему сообщений об ошибках как валидации, так и системных
* Namespace CreateProduct слишком детальный. При таком подходе придётся плодить пространства для всего CRUD
* Разбор входящих данных нужно вынести из CreateProduct. Иначе образуется дополнительная связь.
 